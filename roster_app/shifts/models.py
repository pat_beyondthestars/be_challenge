from django.db import models


class Shift(models.Model):
    FIRST_SHIFT = 1
    SECOND_SHIFT = 2
    THIRD_SHIFT = 3
    SHIFT_CODE_CHOICES = (
        (FIRST_SHIFT, '1st shift'),
        (SECOND_SHIFT, '2nd shift'),
        (THIRD_SHIFT, '3rd shift'),
    )
    name = models.CharField(max_length=25)
    start_time = models.TimeField()
    end_time = models.TimeField()
    break_minutes = models.PositiveIntegerField()
    max_employees_per_day = models.PositiveIntegerField(default=0)
    code = models.IntegerField(
        choices=SHIFT_CODE_CHOICES,
        blank=True,
        null=True,
    )

    def __str__(self):
        return '%s: %s to %s' % (
            self.name,
            self.start_time.strftime('%I:%M %p'),
            self.end_time.strftime('%I:%M %p'),
        )


class Employee(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class ShiftAssignment(models.Model):
    date = models.DateField()
    shift = models.ForeignKey(Shift, on_delete=models.CASCADE)
    employee = models.ForeignKey(
        Employee,
        on_delete=models.CASCADE,
        related_name='shift_assignments',
    )

    def __str__(self):
        return '%s - %s:%s' % (
            self.date,
            self.shift.name,
            self.employee,
        )
