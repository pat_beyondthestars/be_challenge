from django.urls import path

from .api import views

urlpatterns = [
  path('shift_assignments/',
        view=views.ShiftAssignmentListCreateAPIView.as_view(),
        name='create'),
  path('shift_assignments/<int:pk>/',
        view=views.ShiftAssignmentRetrieveUpdateDestroyAPIView.as_view(),
        name='update'),
  path('employees/',
        view=views.EmployeeListCreateAPIView.as_view()),
  path('employees/<int:pk>/',
        view=views.EmployeeRetrieveUpdateDestroyAPIView.as_view()),
]
