from ..models import ShiftAssignment, Shift
from datetime import timedelta
from rest_framework import serializers


def validate_employee_has_no_shifts_for_the_day(employee, date):
    """
    Validate that employee has no other `ShiftAssignment` for that `date`
    """
    employee_has_existing_assignment_for_date = (
        employee.shift_assignments
                .filter(date=date)
                .exists()
    )

    if employee_has_existing_assignment_for_date:
        raise serializers.ValidationError(
            'Employee already has assignment for that date.',
        )


def validate_employee_has_no_back_to_back_shift(employee, date, shift):
    """
    Prevent employees from having back-to-back shifts
    """
    if shift.code == Shift.FIRST_SHIFT:
        day_before = date - timedelta(days=1)

        employee_has_night_shift_yesterday = (
            employee.shift_assignments
                    .filter(date=day_before,
                            shift__code=Shift.THIRD_SHIFT)
                    .exists()
        )

        if employee_has_night_shift_yesterday:
            raise serializers.ValidationError(
                'Employee has night shift the day before.'
            )

    elif shift.code == Shift.THIRD_SHIFT:
        day_after = date + timedelta(days=1)

        employee_has_morning_shift_tomorrow = (
            employee.shift_assignments
                    .filter(date=day_after,
                            shift__code=Shift.FIRST_SHIFT)
                    .exists()
        )

        if employee_has_morning_shift_tomorrow:
            raise serializers.ValidationError(
                'Employee has morning shift the day after.'
            )


def validate_max_shift_count_not_reached(date, shift):
    """
    Prevent exceeding number of employees per shift type
    """
    shift_assignment_count = (
        ShiftAssignment.objects
                       .filter(date=date, shift=shift)
                       .count()
    )
    if shift_assignment_count >= shift.max_employees_per_day:
        raise serializers.ValidationError(
            'Max {shift_name} assignments for {date} already reached: {max_count}'.format(
                shift_name=shift.name,
                date=date,
                max_count=shift.max_employees_per_day,
            )
        )
