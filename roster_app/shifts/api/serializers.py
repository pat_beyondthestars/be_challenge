from rest_framework import serializers

from . import validators
from ..models import Employee, Shift, ShiftAssignment
from .mixins import RepresentationMixin


class ShiftSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shift
        fields = [
            'id',
            'name',
            'start_time',
            'end_time',
            'break_minutes',
        ]


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = [
            'id',
            'first_name',
            'last_name',
        ]


class ShiftAssignmentSerializer(RepresentationMixin, serializers.ModelSerializer):
    class Meta:
        model = ShiftAssignment
        fields = [
            'id',
            'date',
            'shift',
            'employee',
        ]
        nested_serializers = [
            {
                'field': 'employee',
                'serializer_class': EmployeeSerializer,
            },
            {
                'field': 'shift',
                'serializer_class': ShiftSerializer,
            },
        ]

    def validate(self, data):
        date = data.get('date')
        employee = data.get('employee')
        shift = data.get('shift')

        validators.validate_max_shift_count_not_reached(date, shift)
        validators.validate_employee_has_no_shifts_for_the_day(employee, date)
        validators.validate_employee_has_no_back_to_back_shift(employee, date, shift)

        return data
