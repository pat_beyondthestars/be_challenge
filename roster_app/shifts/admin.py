from django.contrib import admin

from .models import Employee, Shift, ShiftAssignment

admin.site.register(Shift)
admin.site.register(ShiftAssignment)
admin.site.register(Employee)
