from datetime import timedelta
from random import choice

from .api.serializers import ShiftAssignmentSerializer
from .models import Employee, Shift


class RosterGenerator:
    def __init__(self, start_date, end_date):
        """
        Parameters
        ----------
        start_date : `datetime.date`
        end_date: `datetime.date`
        """
        self.start_date = start_date
        self.end_date = end_date
        self.all_employee_ids = list(Employee.objects.values_list('id', flat=True))

    def get_all_dates(self):
        """
        Return dates starting at `self.start_date` up to (not including) `self.end_date`
        """
        dates = []
        start_date = self.start_date

        while start_date < self.end_date:
            dates.append(start_date)
            start_date += timedelta(days=1)

        return dates

    def populate_shift(self, shift, date):
        """
        Select random `shifts.models.Employee`s to populate the `shifts.models.Shift`
        for the given `date`.

        Parameters
        ----------
        shift: `shifts.models.Shift`
        date: `datetime.date`
        """
        employee_ids = self.all_employee_ids.copy()
        shift_assignment_data = {
            'date': date,
            'shift': shift.pk,
        }

        while len(employee_ids):
            if shift.max_employees_per_day == shift.shiftassignment_set.filter(date=date).count():
                return

            employee_id = choice(employee_ids)
            employee_ids.remove(employee_id)

            shift_assignment_data['employee'] = employee_id
            shift_assignment = ShiftAssignmentSerializer(data=shift_assignment_data)

            if shift_assignment.is_valid():
                shift_assignment.save()

    def generate_roster(self):
        all_shifts = Shift.objects.all()

        for date in self.get_all_dates():
            for shift in all_shifts:
                self.populate_shift(shift, date)
