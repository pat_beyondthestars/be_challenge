# Project setup

1. Python version: `3.7.0`
2. Install dependencies via `pip install -r requirements.txt` or `pipenv install`
3. Run `python manage.py migrate`
4. Load Employee data: `python manage.py loaddata employees.json`
5. Load Shift data: `python manage.py loaddata shifts.json`
6. Start local server: `python manage.py runserver`

## Solutions to challenges
#### Web API for creating `ShiftAssignment`s

http://localhost:8000/shifts/api/shift_assignments/

- Provide the date, shift and employee. If validations passed, a new `ShiftAssignment` is created.

You can also perform CRUD on employees here: http://localhost:8000/shifts/api/employees/

#### Algorithm which generates an allocation of staff to shifts: `shifts.utils.RosterGenerator`

Enter shell mode: `python manage.py shell`
```
>>> from shifts.utils import RosterGenerator
>>> from datetime import datetime, timedelta
>>> start_date = datetime.today().date()
>>> end_date = start_date + timedelta(days=7)
>>> roster_generator = RosterGenerator(start_date=start_date, end_date=end_date)
>>> roster_generator.generate_roster()
```

# My assumptions:

1. In the `shifts.csv` file, break is stored by minutes
2. All shifts have 60 minutes break
3. Morning shift starts at 5 AM, ends at 1.30 PM
4. Afternoon shift starts at 1 PM, ends at 9.30 PM
5. Night shift starts at 9 PM, ends at 5.30 PM
6. Morning & afternoon shifts require 3 persons.
7. Night shift requires 2 persons.
8. An employee can only have 1 shift per day.
9. This app is only used by the admin.


### Questions to the client:
1. When are the daily rosters prepared? Days, weeks or months before?
2. How are employee day offs determined? What are day offs rules?
3. Do we want to handle time start & end of employee's break during shift?
4. What parameters do you want for the roster generator script? We could make it exclude some employees, or populate only some shifts.


### Could have done better:
1. Maybe Shift model isn't needed, since since they are mostly constant for the company?
2. Update `shifts.utils.RosterGenerator` function once client has provided rules for day offs.
3. Create endpoint that will trigger  `shifts.utils.RosterGenerator.generate_roster`
5. Create tests for the validators and `RosterGenerator`
